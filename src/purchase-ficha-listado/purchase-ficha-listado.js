import { LitElement, html} from 'lit-element';

class PurchaseFichaListado extends LitElement {

    static get properties() {
        return {
            id: {type: String},
            userid: {type: String},
            amount: {type: Number},
            purchaseItems: {type: Object}
        };
    }

    constructor() {
        super();

    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="card h-100">                
                <div class="card-body">
                    <h5 class="card-title"><label>Id Pedido: </label>${this.id}</h5>
                    <h5 class="card-title"><label>Cliente: </label>${this.userid}</h5>
                    <p class="card-text">
                        <label>Items: </label>
                            ${ this.purchaseItems }
                    </p>
                    <h3 class="card-title"><strong><label>Cantidad: </label>${this.amount}&nbsp;€</strong></h3>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePurchase}" class="btn btn-danger col-5"><strong>X</strong></button>
                </div>
            </div>
        `;
    }

    deletePurchase(e) {
        console.log("purchase-ficha-listado.js deletePurchase");
        console.log("Se va a borrar el coche id " + this.id);

        this.dispatchEvent(
            new CustomEvent("delete-purchase", {
                detail: {
                    id: this.id
                }
            })
        );
    }

}

customElements.define('purchase-ficha-listado', PurchaseFichaListado)