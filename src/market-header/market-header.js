import { LitElement, html } from "lit-element";

class MarketHeader extends LitElement{

  static get properties(){
    return {
    };
  }

  constructor(){
    super();
  }

  render(){
    return html`
      <h1 class="font-weight-bold text-center text-nowrap">Car Market</h1>
    `;
  }
}

customElements.define('market-header', MarketHeader);
