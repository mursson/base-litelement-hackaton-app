import { LitElement, html} from 'lit-element';

class PurchaseDM extends LitElement {

    static get properties() {
        return {
            purchases : {type: Array}
        };
    }

    constructor() {
        super();

        this.purchases = [];

        this.getPurchasesData();
    }

    
    updated(changedProperties) {
        console.log("purchase-dm.js updated");

        if(changedProperties.has("purchases")) {
            console.log("purchase-dm.js Ha cambiado el valor de propiedad purchase");

            this.dispatchEvent(
                new CustomEvent(
                    "purchases-data-updated",
                    {
                        detail: {
                            purchases: this.purchases
                        }
                    }
                )
            )
        }
    }

    getPurchasesData() {
        console.log("purchase-test-api.js Dentro del getPurchasesData");
        console.log("LLAMADA A BACKEND --> Obteniendo datos de purchases del API");

        //DOCUMENTACIÓN Interesante: https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest
        let xhr = new XMLHttpRequest();
  
        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

                //console.log(xhr.responseText);
                console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);

                //No podemos asumir que la respuesta de un API sea siempre con este esquema de datos en results
                this.purchases = APIResponse;

                console.log(APIResponse);

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("GET","http://localhost:8080/hackaton/purchases");
        xhr.send();

        console.log("purchase-test-api.js Fin del getPurchasesData");
    }

}

customElements.define('purchase-dm', PurchaseDM)