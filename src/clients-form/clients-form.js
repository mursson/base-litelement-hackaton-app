import { LitElement, html } from "lit-element";

class ClientsForm extends LitElement{

  static get properties(){
    return {
      client: {type: Object},
      editingClient: {type: Boolean}
    };
  }

  constructor(){
    super();
    console.log("constructor clients-form");
    this.resetFormData();
  }

  render(){
    // en atributos de presencia para controlar si ponerlo o no (ejemplo disabled), se pone delante ? y se asocia un booleano.
    return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <div>
        <form>
          <div class="form-group">
            <label>Nombre Completo</label>
            <input @input="${this.updateName}"
              type="text"
              class="form-control"
              placeholder="Nombre completo"
              .value="${this.client.name}"
            />
          </div>
          <div class="form-group">
            <label>Edad</label>
            <input @input="${this.updateAge}"
              type="text"
              class="form-control"
              placeholder="Edad"
              .value="${this.client.age}"
            />
          </div>
          <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
          <button @click="${this.storeClient}" class="btn btn-success"><strong>Guardar</strong></button>
        </form>
      </div>
    `;
  }

  updateName(e){
    console.log("updateName");
    console.log("Actualizando la propiedad name con el valor " + e.target.value);

    this.client.name = e.target.value;
  }

  resetFormData(){
    console.log("resetFormData");

    this.client = {};
    this.client.id = "";
    this.client.name = "";
    this.client.age = "";
    this.editingClient = false;
  }

  updateAge(e){
    console.log("updateAge");
    console.log("Actualizando la propiedad age con el valor " + e.target.value);

    this.client.age = e.target.value;
  }

  goBack(e){
    console.log("goBack");
    e.preventDefault();

    this.resetFormData();
    this.dispatchEvent(new CustomEvent("client-form-close",{}));
  }

  storeClient(e){
    console.log("storeClient");
    e.preventDefault();

    console.log("La propiedad id vale " + this.client.id);
    console.log("La propiedad name vale " + this.client.name);
    console.log("La propiedad age vale " + this.client.age);
    console.log("La propiedad editingClient vale " + this.editingClient);

    let idFinal = this.editingClient ? this.client.id : Math.floor(Math.random()*1000)+1;
    console.log ("idFinal = " + idFinal);

    this.dispatchEvent(
      new CustomEvent(
        "client-form-store",
        {
          detail :{
            client :{
              id: idFinal,
              name: this.client.name,
              age: this.client.age,
            },
            editingClient : this.editingClient
          }
        }
      )
    );
    this.resetFormData();
  }

}

customElements.define('clients-form', ClientsForm);
