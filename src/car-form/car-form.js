import { LitElement, html} from "lit-element";

class CarForm extends LitElement {
    
    static get properties() {
        return {
            car: {type: Object},
            editingCar: { type: Boolean }
        };
    }

    constructor() {
        super();

        this.car = {};

        this.resetFormData();
        
    }

    render() {
        return html`
            <!-- Enlace Bootstrap -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                <div class="form-group">
                        <label>ID</label>
                        <input type="text" 
                            @input="${this.updateId}" 
                            id="carFormId" 
                            class="form-control" 
                            placeholder="ID" 
                            .value="${this.car.id}" 
                            ?disabled="${this.editingCar}" 
                            />
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <input type="text" 
                            @input="${this.updateDesc}" 
                            id="carFormDesc" 
                            class="form-control" 
                            placeholder="Descripción" 
                            .value="${this.car.desc}" 
                            ?disabled="${this.editingCar}" 
                            />
                    </div>
                    <div class="form-group">
                        <label>Color</label>
                        <input type="text" 
                            id="exampleColorInput" 
                            @input="${this.updateColor}" 
                            class="form-control" 
                            placeholder="Color" 
                            .value="${this.car.color}" 
                            ?disabled="${this.editingCar}" 
                            />
                    </div>
                    <div class="form-group">
                        <label>Precio</label>
                        <input type="number" 
                             @input="${this.updatePrecio}" 
                             class="form-control" 
                             placeholder="Precio" 
                             .value="${this.car.price}" 
                             ?disabled="${this.editingCar}" 
                             />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeCar}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent( new CustomEvent("car-form-close",{}));
    }

    updateId(e) {
        console.log("car-form.js - updateName");
        console.log("car-form.js - Actualizando la propiedad id con el valor "+ e.target.value);
        this.car.id = e.target.value;
    }

    updateDesc(e) {
        console.log("car-form.js - updateDesc");
        console.log("car-form.js - Actualizando la propiedad desc con el valor "+ e.target.value);
        this.car.desc = e.target.value;
    }

    updateColor(e) {
        console.log("car-form.js - updateColor");
        console.log("car-form.js - Actualizando la propiedad color con el valor "+ e.target.value);
        this.car.color = e.target.value;
    }
    
    updatePrecio(e) {
        console.log("car-form.js - updatePrecio");
        console.log("car-form.js - Actualizando la propiedad price con el valor "+ e.target.value);
        this.car.price = e.target.value;
    }

    resetFormData(e) {
        console.log("car-form.js - resetFormData");
        console.log("car-form.js - Reseteando los input");

        this.car = {};
        this.car.id = "0";
        this.car.desc = "";
        this.car.foto = "";
        this.car.price = 0;
        this.car.color = "";

        this.editingCar = false;
    }

    storeCar(e) {
        console.log("car-form.js - storeCar");
        e.preventDefault();

        // this.car.foto = {
        //     "src": "./img/car.jpg",
        //     "alt": "car"
        // }
        this.car.foto = "./img/car.jpg";
        //this.car.foto = "";

        console.log("car-form.js - La propiedad id vale " + this.car.id);
        console.log("car-form.js - La propiedad name vale " + this.car.desc);
        console.log("car-form.js - La propiedad color vale " + this.car.color);
        console.log("car-form.js - La propiedad price vale " + this.car.price);

        this.dispatchEvent( new CustomEvent("car-form-store", {
            detail: {
                car : {
                    id: this.car.id,
                    desc: this.car.desc,
                    color: this.car.color,
                    price: this.car.price,
                    foto: this.car.foto
                },
                editingCar :  this.editingCar
            }
        }));

        this.resetFormData();
    }
}

customElements.define('car-form', CarForm)