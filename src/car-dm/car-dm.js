import { LitElement, html} from 'lit-element';

class CarDM extends LitElement {

    static get properties() {
        return {
            cars : {type: Array}
        };
    }

    constructor() {
        super();

        this.cars = [];

        this.getCarsData();
    }

    // render() {
    //     return html`
    //     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    //     <h1>Car App</h1>
    //     `;
    // }
    
    updated(changedProperties) {
        console.log("car-dm.js updated");

        if(changedProperties.has("cars")) {
            console.log("car-dm.js Ha cambiado el valor de propiedad car");

            this.dispatchEvent(
                new CustomEvent(
                    "cars-data-updated",
                    {
                        detail: {
                            cars: this.cars
                        }
                    }
                )
            )
        }
    }

    getCarsData() {
        console.log("car-test-api.js Dentro del getCarsData");
        console.log("LLAMADA A BACKEND --> Obteniendo datos de coches del API");

        //DOCUMENTACIÓN Interesante: https://developer.mozilla.org/es/docs/Web/API/XMLHttpRequest
        let xhr = new XMLHttpRequest();
        
        //Ejemplo de async (se puede hacer después!)
        //Puñetas => console.log(this)
        //Aquí podemos programar comportamientos en función de la respuesta que recibamos
        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

                //console.log(xhr.responseText);
                console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);

                //No podemos asumir que la respuesta de un API sea siempre con este esquema de datos en results
                this.cars = APIResponse;

                console.log(APIResponse);

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("GET","http://localhost:8080/hackaton/cars");
        xhr.send();

        console.log("car-test-api.js Fin del getCarsData");
    }

    // deleteCar(e) {
    //     console.log("car-test-api.js Dentro del deleteCar");

    //     let xhr = new XMLHttpRequest();

    //     xhr.onload = () => {
    //         if (xhr.status === 200){
    //             //Ejemplo de async -> este log sale lo último en la carga de la página
    //             console.log("STATUS 200 - OK - Petición realizada correctamente");

    //             //console.log(xhr.responseText);
    //             console.log(JSON.parse(xhr.responseText));

    //             let APIResponse = JSON.parse(xhr.responseText);

    //             //No podemos asumir que la respuesta de un API sea siempre con este esquema de datos en results
    //             this.cars = APIResponse;

    //             console.log(APIResponse);

    //         }else if (xhr.status === 400){
    //             //Ejemplo de async -> este log sale lo último en la carga de la página
    //             console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

    //         }else if (xhr.status === 404){
    //             //Ejemplo de async -> este log sale lo último en la carga de la página
    //             console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
    //         }
    //     }

    //     xhr.open("DELETE","http://localhost:8080/hackaton/cars/");
    //     xhr.send();
    // }

}

customElements.define('car-dm', CarDM)