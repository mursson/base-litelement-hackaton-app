import { LitElement, html } from "lit-element";
import '../clients-dm/clients-dm.js';
import '../clients-ficha/clients-ficha.js';
import '../clients-form/clients-form.js';

class ClientsMain extends LitElement{

  static get properties(){
    return {
      clients : {type: Array},
      showClientForm : {type: Boolean}
    };
  }

  constructor(){
    super();
    console.log("constructor clients-main");
    this.clients = [];
    this.showClientForm = false;
  }

  updated(changedProperties){
    console.log("updated en clients-main");

    if (changedProperties.has("showClientForm")){
      console.log("Ha cambiado el valor de la propiedad showClientForm en clients-main");

      if (this.showClientForm === true){
        this.showClientFormData();
      }else{
        this.showClientList();
      }
    }

    if (changedProperties.has("people")){
      console.log("Ha cambiado el valor de la propiedad people en persona-main");

      this.dispatchEvent(
        new CustomEvent(
          "updated-people",
          {
            detail :{
              people : this.people
            }
          }
        )
      );
    }
  }

  render(){
    return html`
      <clients-dm
        @clients-data-updated="${this.clientsDataUpdated}"
      ></clients-dm>

      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <h2 class="text-center">Clientes</h2>
        <div class="row" id="clientList">
          <div class="row row-cols-1 row-cols-sm-4">
            ${this.clients.map(
              client => html`<clients-ficha
                id="${client.id}"
                name="${client.name}"
                age="${client.age}"
                @delete-client="${this.deleteClient}"
                @info-client="${this.infoClient}"
              ></clients-ficha>`
            )}
          </div>
        </div>

        <div class="row">
        <clients-form
          @client-form-close="${this.clientFormClose}"
          @client-form-store="${this.clientFormStore}"
          class="d-none border rounded border-primary"
          id="clientForm">
        </clients-form>
      </div>
    `;
  }

  clientsDataUpdated(e){
    console.log("clientsDataUpdated en clients-main");
    this.clients = e.detail.clients;
  }

  deleteClient(e){
    console.log("deleteClient en clients-main");
    console.log (e.detail);
    this.shadowRoot.querySelector("clients-dm").deleteClientId = e.detail.id;
  }

  clientFormClose(){
    console.log("clientFormClose");
    console.log("Se ha cerrado el formulario del cliente");

    this.showClientForm = false;
  }

  infoClient(e){
    console.log("infoClient en clients-main");
    console.log (this.clients);
    console.log (e.detail);

    let chosenClient = this.clients.filter(
      cli => cli.id === e.detail.id
    );

    console.log("cliente seleccionado: " + chosenClient);

    let client = {};
    client.id = chosenClient[0].id;
    client.name = chosenClient[0].name;
    client.age = chosenClient[0].age;

    this.shadowRoot.getElementById("clientForm").client = client;
    this.shadowRoot.getElementById("clientForm").editingClient = true;
    this.showClientForm = true;

  }

   clientFormStore(e){
    console.log("personFormStore");
    console.log("Se va a almacenar un cliente");
    console.log(e.detail.client);

    if (e.detail.editingClient === true){
      console.log("Se va a actualizar el cliente " + e.detail.client.name);
      this.shadowRoot.querySelector("clients-dm").modifyClient = e.detail.client;

    }else{
      console.log("Se va a almacenar un cliente nuevo");
      this.shadowRoot.querySelector("clients-dm").newClient = e.detail.client;
    }

    console.log("Cliente almacenado");

    this.showClientForm = false;
  }

  showClientFormData(){
    console.log("showClientFormData");
    console.log("Mostrando formulario de un cliente");
    this.shadowRoot.getElementById("clientForm").classList.remove("d-none");
    this.shadowRoot.getElementById("clientList").classList.add("d-none");
  }

  showClientList(){
    console.log("showClientList");
    console.log("Mostrando listado de clientes");
    this.shadowRoot.getElementById("clientForm").classList.add("d-none");
    this.shadowRoot.getElementById("clientList").classList.remove("d-none");
  }

}

customElements.define('clients-main', ClientsMain);
